NAME := radio-toolbox
TAG = $(shell git describe --abbrev=0)
INSTALL = /usr/bin/install -c
DEB_BRANCH = debian/sid

prefix = /usr
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
datarootdir = $(prefix)/share
docdir = $(datarootdir)/doc/$(NAME)-$(TAG)
mandir = $(datarootdir)/man
applicationsdir = $(datarootdir)/applications
iconsdir = ${datarootdir}/icons
DESTDIR =

MAIN_SCRIPT = radio-toolbox
REDIRECTED_SCRIPTS = nouveau_projet ouvrir_projet
BIN_FILES = $(addprefix $(DESTDIR)$(bindir)/,$(REDIRECTED_SCRIPTS))
DESKTOP_FILES = $(addsuffix .desktop,$(REDIRECTED_SCRIPTS))
ICON_FILES = $(addsuffix .svg,$(REDIRECTED_SCRIPTS))

.PHONY: all info clean install uninstall deb snapshot release
all: info

info:
	$(info Toolbox du studio radio $(TAG))

install: info
	$(INSTALL) -d -m 755 $(DESTDIR)$(bindir)
	$(INSTALL) -t $(DESTDIR)$(bindir) -m 755 $(MAIN_SCRIPT)
	for link in $(BIN_FILES); do ln -sv $(bindir)/$(MAIN_SCRIPT) $$link; done;
	$(INSTALL) -d -m 755 $(DESTDIR)$(applicationsdir)
	$(INSTALL) -t $(DESTDIR)$(applicationsdir) -m 644 $(DESKTOP_FILES)
	$(INSTALL) -d -m 755 $(DESTDIR)$(iconsdir)
	$(INSTALL) -t $(DESTDIR)$(iconsdir) -m 644 $(ICON_FILES)
	$(INSTALL) -d -m 755 $(DESTDIR)$(docdir)
	$(INSTALL) -t $(DESTDIR)$(docdir) -m 644 COPYING README

uninstall: info
	-rm $(DESTDIR)$(bindir)/$(MAIN_SCRIPT)
	-rm $(BIN_FILES)
	-rm $(DESTDIR)$(docdir)/COPYING $(DESTDIR)$(docdir)/README
	-for desktop_file in $(DESKTOP_FILES); do rm $(DESTDIR)$(applicationsdir)/$$desktop_file; done;
	-for icon_file in $(ICON_FILES); do rm $(DESTDIR)$(iconsdir)/$$icon_file; done;

deb: info
	git rebase $(TAG) $(DEB_BRANCH)
	gbp buildpackage --git-pristine-tar --git-pristine-tar-commit -us -uc

snapshot: info
	git fetch
	git checkout $(DEB_BRANCH)
	git merge $(TAG)
	gbp dch --snapshot --auto debian/
	gbp buildpackage --git-ignore-new --git-pristine-tar --git-pristine-tar-commit -us -uc

release: info
	gbp dch --commit --release --auto
	gbp buildpackage -us -uc

clean: info
	-rm -r ./build
